**Table of Contents**

[[_TOC_]]

## **1. Fundamentals**


### 1.1 Description
Set analysis offers a way of defining a set (or group) of data values that is different from the normal set defined by the current selections. So that advanced expressions can be created.

___
### 1.2 Scope
- Set analysis can be used only as an aggregation function

___
### 1.3 Structure

<a name='set-expression'></a>

#### 1.3.1 Set Analysis and Set Expression Difference
Set analysis can be built by defining set expression within an aggregation function. Here is how it looks:

![](images/SetAnalysis_SetExpression.png "Set Analysis and Set Expression Difference")

Here **Sum( {$<Year={2019}>} Revenue )** is set analysis, and **{$<Year={2019}>}** is set expression.

<a name='set-expression-components'></a>
#### 1.3.2 Set Expression Components
A set expression starts and ens with curly brackets {} and consists of a combination of the following parts:

![](images/Set_Expression_Components.png "Set Expression Components")

- **[Set Identifier](#set-identifier)**

    Identifiers define the relationship between the set expression and what is being evaluated in the rest of the expression.

- **[Set Operator](#set-operator)**

    If there is more than one identifier, an operator or operators are used to refine the set of data by specifying how the sets of data represented by the identifiers are combined to create a subset or superset.

- **[Set Modifier](#set-modifier)**

    A modifier or modifiers can be added to the set expression to change the selection. A modifier can be used on its own or to modify an identifier to filter the data set. They act like WHERE clause in SQL. A set modifier starts and ens with angle brackets <>


Now let's deep dive into set expression components.

Let's say we have a simple sales table for 2 customers, 2 product categories and revenue and discount amounts distributed into 3 years.


|Order_Id|Date	|Customer_Name	|Product_Category	|Product_Name	|Revenue|Discount|
|:--:| ----- |:-------------:|:-----------------:|:-------------:|:-----:|:------:|
|1|01/03/2020|Alfreds Futterkiste|Confections|Schoggi Schokolade|1500|160|
|2|01/04/2019|Alfreds Futterkiste|Confections|Teatime Chocolate Biscuits|4500|250|
|3|01/06/2018|Alfreds Futterkiste|Confections|Zaanse koeken|3000|200|
|4|01/03/2020|Alfreds Futterkiste|Meat/Poultry|Thüringer Rostbratwurst|2000|100|
|5|01/04/2019|Alfreds Futterkiste|Meat/Poultry|Mishi Kobe Niku|3500|120|
|6|01/06/2018|Alfreds Futterkiste|Meat/Poultry|Perth Pasties|2500|240|
|7|01/03/2020|Bottom-Dollar Markets|Confections|Schoggi Schokolade|500|50|
|8|01/04/2019|Bottom-Dollar Markets|Confections|Teatime Chocolate Biscuits|2000|140|
|9|01/06/2018|Bottom-Dollar Markets|Confections|Maxilaku|1500|70|
|10|01/03/2020|Bottom-Dollar Markets|Meat/Poultry|Thüringer Rostbratwurst|3000|210|
|11|01/04/2019|Bottom-Dollar Markets|Meat/Poultry|Mishi Kobe Niku|2500|190|
|12|01/06/2018|Bottom-Dollar Markets|Meat/Poultry|Perth Pasties|1000|50|

We will use this dataset for the set expression component examples.

<a name='set-identifier'></a>
#### 1.3.2.1 Set Identifier

| Identifier    | Description                                  |
|:-------------:|:--------------------------------------------:|
| 1             | Represents the full set of all the records in the application, irrespective of any selections made.                                |
| $             | Represents the records of the current selection. It is default identifier selection by QlikSense. If the identifier is not stated, then $ will be used                                        |
| $\<number>            | $1 the previous selection. $2 represents the previous selection-but-one, and so on.                                    |
| $_\<number>             | $_1 represents the next (forward) selection. $_2 represents the next selection-but-one, and so on                                     |
| \<bookmark Id> or \<bookmark name>             | Represents the records of the selections made in bookmark.  

***Identifier Examples***

**Usage of 1 as identifier:**

It means any selections will be disregarded. Let's say we have applied some filters. When we use 1 as identifier in our set analysis, the result will not change and it will always bring 27.5k

```
Sum({1}Revenue)
```

**Usage of $ as identifier or not stating an identifier:**

This is the default behaviour of set analysis. It will bring the results for current selection. It doesn't make sense using it without any operator or modifier.

```
Sum({$}Revenue)
```

**Usage of a bookmark as identifier:**

Let's assume that we have a bookmark named *Bookmark_1* with Year=2019 and Customer_Name='Alfreds Futterkiste' selections. The results will be 8k disregarding any current selection.

```
Sum({Bookmark_1}Revenue)
```

<a name='set-operator'></a>
#### 1.3.2.2 Set Operator


Operators are used to include, exclude, or intersect parts of or whole data sets. All operators use sets as operands and return a set as result.

| Operator      | Description                                  |
|:-------------:|:--------------------------------------------:|
|+<br><br>![](images/union_venn.png "Union Image")|Union. This binary operation returns a set consisting of the records that belong to any of the two set operands.|
|-<br><br>![](images/exclusion_venn.png "Exclusion Image")|Exclusion. This binary operation returns a set of the records that belong to the first but not the other of the two set operands.|
|*<br><br>![](images/intersection_venn.png "Intersection Image")|Intersection. This binary operation returns a set consisting of the records that belong to both of the two set operands.|
|/<br><br>![](images/xor_venn.png "XOR Image")|Symmetric difference (XOR - Logical exclusive OR). This binary operation returns a set consisting of the records that belong to either, but not both of the two set operands.|
<br><br><br><br>

| Advanced Operator      | Description                                  |
|:----------------------:|:--------------------------------------------:|
|-=|Excludes a set of specific value or multiple values from user selections.|
|=-|Excludes a set of specific value or multiple values from the entire data set.|
|+=|Adds a set of specific value or multiple values to user selections.|
|*=|Excludes set of values that are not common between left hand side set and right hand side set.|



***Operator Examples***

| Example             | Result                                   |
|:--------------------|:-----------------------------------------|
|Sum({1-$}Revenue)|Returns revenue for everything excluded current selection. In our example when we select 2019 on 'Year' filter, the result will be 15k|
|Sum({$\*Bookmark_1}Revenue)|Returns revenue for the intersection between the selection and bookmark Bookmark_1. <br><br>In this case without using a modifier, the value will be 0 if the result of the current filter do not cover the subset of the bookmark, in our example Year=2019 and Customer_Name='Alfreds Futterkiste'. And the result will be 8k if current selection covers the subset of the bookmark.|
|Sum({$+Bookmark_1}Revenue)|Returns revenue for the union of the current selection and Bookmark_1.<br><br>When we select 2020 from Year filter, the result will be 15k: Current Selection(7k) + Bookmark_1(8k)<br><br>If no selection is made, QlikSense brings full dataset by default and the result will be 27.5k |
|If(<br>&nbsp;&nbsp;IsNull(GetCurrentSelections()),<br>&nbsp;&nbsp;Sum({Bookmark_1}Revenue),<br>&nbsp;&nbsp;>Sum({$ + Bookmark_1}Revenue))|This is similar to the expression above: Sum({$+Bookmark_1}Revenue) but if no selection is made, the result will be the subset of the bookmark. (8k)|
|Sum({-($ + Bookmark_1)}Revenue)|Returns revenue excluded by the selection and bookmark Bookmark_1.<br><br>When we select 2020 from Year filter, the result will be 12.5k: (Total(27.5k) - (Current Selection(7k) + Bookmark_1(8k))|
|Sum({$/Bookmark_1}Revenue)|Returns the symmetric difference between the current selection and bookmark Bookmark_1.<br><br>When 2020 is selected as Year, the result will be 15k: (Current Selection(7k) + Bookmark_1(8k)) Because there is no common values between these two subsets and bitwise XOR (Logical exclusive OR) operator returns a set consisting of the records that belong to either, but not both of the two set operands.<br><br>When 2019 and 2020 is selected as Year, the result will be 11.5k: (2020 Revenue(7k) + 2019 Revenue for Bottom-Dollar Markets(4.5k)) Common subset between current selection and Bookmark_1 is 2019 Revenue for Alfreds Futterkiste. And it will be excluded from the result<br><br>![](images/bitwise_xor.jpeg "XOR Truth Table")|
|Sum({<\[Product_Name] -= {'Schoggi Schokolade'}>} \[Revenue])|Returns revenue by excluding 'Schoggi Schokolade' product name from user selections. When this product is selected, the result will be 0.|
|Sum({<\[Product_Name] =- {'Schoggi Schokolade'}>} \[Revenue])|Returns revenue by excluding 'Schoggi Schokolade' product name from entire data set. When this product is selected, the result will be total revenue except this product.|
|Sum({<\[Product_Name] += {'Schoggi Schokolade'}>} \[Revenue])|Returns revenue by adding 'Schoggi Schokolade' product name from user selections.|
|Sum({<\[Product_Name] *= {"Sch*"}+{"Tea*"}>} \[Revenue])|Returns revenue for the intersection of product names starting with 'Sch' and 'Tea'.<br><br>If no selection is made, the result will be 8.5k.<br>If only 'Schoggi Schokolade' is in user selections, the result will be 2k<br>If only 'Teatime Chocolate Biscuits' is in user selections, the result will be 6.5k<br>If both of them are in user selections, the result will be 8.5k|
|Sum({<\[Product_Name] -= {'Schoggi Schokolade', 'Teatime Chocolate Biscuits'}>} \[Revenue])|Returns revenue by excluding 'Schoggi Schokolade' and 'Teatime Chocolate Biscuits' product name from user selections.|

<a name='set-modifier'></a>
#### 1.3.2.3 Set Modifier

Modifiers are used to make additions or changes to a selection. They begin and end with angle brackets, <>.

The inner curly brackets in set modifier define the value elements.

> If inner curly brackets are not defined, then any selections on the stated field (filed on left hend side of equal sign) will be ignored.

![](images/Set_Modifier_Value_Element.png "Set Modifier Value Element")


The rules to state value elements are;

- Numerical values can be stated with or without single quotes:
    
    - 2019 or '2019'

- String values should be stated in single quotes:
    
    - <\Product_Category = {'Confections'}>

- Wildcard search can be done by using double quotes:

    - <\Product_Category = {"C*"}> brings data for the product categories starting with 'C'

- Range search can be done by using double quotes:

    - <\[Date.autoCalendar.Date] = {"<=31/12/2019"}>


> QlikSense is case sensitive so string value has to match actual value in the data set.


Modifiers can contain modifiers (Nested modifiers).

> Sub modifiers in nested modifiers should start with equal sign within the double quotes. Since auto complete feature will not help for the statements within double quotes, practical way to create nested modifiers is, writing them in different lines and then moving them into parent modifier.

![](images/Nested_Modifier.png "Nested Modifiers")


***Modifier Examples***

| Example                           | Result                          |
|:----------------------------------|:--------------------------------|
|Sum({1<\[Date.autoCalendar.Year] = {2019}>}Revenue)|Returns revenue for Year selection as 2019, disregarding the current selection. The result will be 12.5k|
|Sum({1<\[Product_Category] = {'Confections'}>}Revenue)|Returns revenue for Product_Category selection as Confections, disregarding the current selection. The result will be 13k|
|Sum({1<\[Product_Category] = {"C*"}>}Revenue)|Returns revenue for Product_Categories starting with 'C', disregarding the current selection. The result will be 13k|
|Sum({1<\[Date.autoCalendar.Year] = {">=01/01/2019"}>}Revenue)|Returns revenue for Years starting with '201', disregarding the current selection (2010-2019). The result will be 20.5k|
|Sum({1<\[Date.autoCalendar.Date] = {">=01/01/2018 <=31/12/2019"}>}Revenue)|Returns revenue for dates between 01/01/2018 and 31/12/2019. The result will be 20.5k|
|Sum({$<\[Product_Category] = >}Revenue)|Returns revenue for the current selection, but any selection on the Product_Category will be ignored. ($ sign can be omitted.)|
|Sum({1<\[Date.autoCalendar.Year] = {2019}, Product_Category = {'Confections'}>}Revenue)|Multiple modifiers with AND condition.<br><br>Returns revenue for Year selection as 2019, and Product_Category selection as 'Confections' ,disregarding the current selection. It is intersection of two conditions. The result will be 6.5k|
|Sum({1<\[Date.autoCalendar.Year] = {2019}>+<\[Product_Category] = {'Confections'}>}Revenue)|Multiple modifiers with OR condition. It can be done by using '+' operator between modifiers.<br><br>Returns revenue for Year selection as 2019 or Product_Category selection as 'Confections', disregarding the current selection.<br><br>The result will be 19k: (2019 Revenue(12.5k) + Confections Revenue for the Other Years(6.5k))|
|Sum({1<\[Customer_Name] = {<br>&nbsp;&nbsp;"=Sum({1<\[Product_Category] = {'Confections'}>} Revenue) > 5000"}><br>&nbsp;&nbsp;} Revenue)|Returns the revenue for the customers with more than 5000 'Confections revenue.<br><br>The result will be 17k: Revenue for Alfreds Futterkiste'.|
|Sum({<br><br>&nbsp;&nbsp;1<\[Customer_Name] = {"=Sum(Revenue) > 10000"}><br><br>&nbsp;&nbsp;* <\[Customer_Name] = {"=Sum({1<\[Product_Category] = {'Confections'}>} Revenue) < 5000"}><br><br>&nbsp;&nbsp;} Revenue)|Returns the revenue for the customers with more than 10.000 total revenue and less than 5.000 'Confections' revenue.<br><br>The result will be 10.5k: Total revenue of Bottom-Dollar Markets.|
|Sum({1<\[Customer_Name] =<br><br>&nbsp;&nbsp;P({1<\[Product_Category] = {'Confections'}>} \[Customer_Name])<br><br>&nbsp;&nbsp;* E({1<\[Product_Name] = {'Maxilaku'}>} \[Customer_Name])><br><br>} \[Revenue])|Returns the total revenue for the (possible) customers which 'Confections' revenue but not (excluded) 'Maxilaku' product revenue.<br><br>The result will be 17k: Total revenue of Alfreds Futterkiste|
|Sum({1<\[Customer_Name] = <br><br>&nbsp;&nbsp;P({1<\[Product_Category] = {'Confections'}>} \[Customer_Name])<br><br>&nbsp;&nbsp;* E({1<\[Product_Name] = {'Maxilaku'}>} \[Customer_Name])><br><br>&nbsp;&nbsp;* 1<\[Product_Category] = {'Confections'}><br><br>} \[Revenue])|Returns the 'Confections' revenue for the (possible) customers which has 'Confections' revenue but not 'Maxilaku' (excluded) product revenue.<br><br>The result will be 9k : 'Confections' revenue of Alfreds Futterkiste|
|Sum({<\[Date.autoCalendar.Year] = {'$(=$(vMaxYear))'}>} Revenue)|Usage of variable within set modifier. The result will be the total revenue for the max year selected.<br><br>vMaxYear=Max(Year(\[Date.autoCalendar.Year]))|
|$($(=eCalc))|Returns the calculation for the selected measure. Measures are defined in eCalc table. And for each measure, a variable is defined.<br><br>![](images/eCalc_table.png "eCalc Table")<br><br>eRevenue=Sum(Revenue)<br>eDiscount=Sum(Discount)|
|Sum({<\[Order_Id] = {"= Discount > Revenue * 0.1"}>} \[Discount])|Returns discount for orders which have discount more than %10 of revenue.<br><br>A unique key (Order_Id) should be used to compare fields.<br><br>The result will be 160: Discount amount for Order_Id=1|
|Sum({1<\[Product_Name] = {"=Rank(Sum(\[Revenue])) <= 3"}>} \[Revenue])|Returns sum of revenue for the top 3 selling product names.<br><br>The result will be 17.5k:<br>Teatime Chocolate Biscuits(6.5k) + Mishi Kobe Niku(6k) + Thüringer Rotbratwurst(5k)|
|Sum({1<\[Order_Id] = {"=Rank(Sum(\[Revenue])) <= 3"}>} \[Revenue])|Returns sum of revenue for the top 3 selling product names per customer.<br><br>Since there is a tie on 3rd rank, it returns the revenue for 4 customer-product: 14k<br>1<sup>st</sup> - Alfreds Futterkiste-Teatime Chocolate Biscuits(4.5k)<br>2<sup>nd</sup> - Alfreds Futterkiste-Mishi Kobe Niku(3.5k)<br>3<sup>rd</sup> and 4<sup>th</sup> - Alfreds Futterkiste-Zaanse koeken(3k) and Bottom-Dollar Markets-Thüringer Rostbratwurst(3k)
|Count({1-$<\[Flag_1] = {"*"}>} \[Order_Id])|Returns count of orders without null value in Flag_1 field.<br><br>Alternatively;<br>Count({1-$<\[Order_Id] = P({<\[Flag_1] = {"*"}>})>} \[Order_Id])<br>can be used.|


___
<a name='useful-functions'></a>
### 1.4 Set Analysis with Useful Functions

<a name='aggr'></a>
#### 1.4.1 Aggr()

* Aggr() function creates a virtual table during the [expression](#set-expression) evaluation by returning a set of values of expression calculated over dimension(s)
* Once the temporary table is created, it can be used by an outer aggregation function.
* Aggr() function is similar to SQL Groupby clause.
* Aggr() function is implicitly DISTINCT
* Aggr() function is a only UI (frontend) function. It cannot be used in script (backend).

```
//OuterAggregation(Aggr(InnerAggregation(Measure), Dimensions))

//The expression below will return max revenue grouped by customer name:
//Total revenue of Alfreds Futterkiste - 17k

Max(Aggr(Sum(Revenue), [Customer_Name]))
```


***Aggr() Function Examples***

| Example                | Description                                  |
|:-----------------------|:---------------------------------------------|
|Min(Aggr(Sum(Revenue), \[Customer_Name], \[Product_Name]))|Returns min revenue grouped by customer name and product name: Total revenue for Bottom-Dollar Markets and Schoggi Schokolade.|
|Sum(If (Aggr(Avg({<\[Product_Category] = {'Confections'}>}\[Revenue]), \[Customer_Name], \[Product_Name]) > 2500, 1, 0))|Returns count of average revenue per customer-product above 2.5k.|


<a name='firstsortedvalue'></a>
#### 1.4.2 FirstSortedValue()

* FirstSortedValue() can be used in both Script (backend) and UI (frontend).
* It returns a value corresponding to the record/row of sort-weight field.
* If more than resulting value shares the same sort-weight for the specified rank, the function returns NULL
* When a minus sign is placed in front of sort-weight, then function uses descending sorting.

```
//The expression below returns product name with 2nd lowest total revenue:
//Perth Pasties(3.5k)

FirstSortedValue([Product_Name], [Revenue], 2)

//The expression below returns product name with 2nd highest total revenue:
//Mishi Kobe Niku(6k)

FirstSortedValue([Product_Name], - [Revenue], 2)
```


***FirstSortedValue() Function Examples***

| Example                | Description                                  |
|:-----------------------|:---------------------------------------------|
|FirstSortedValue(\[Product_Name], Aggr(Sum(\[Revenue]), \[Customer_Name], \[Product_Name]), 2)|Returns 2nd lowest selling product by customer.<br><br>The result will be: Perth Pasties(1k)|
|FirstSortedValue(\[Product_Name], - Aggr(<br>&nbsp;&nbsp;Sum({<\[Product_Category] = {'Confections'}>}\[Revenue]), <br>&nbsp;&nbsp;\[Customer_Name], \[Product_Name]), 2)|Returns 2nd highest selling product by customer in 'Confections' category.<br><br>The result will be Zaanse koeken(3k)|