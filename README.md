# QlikSense Set Analysis

Welcome!

Purpose of this repository is providing an overarching source for *QlikSense Set Analysis*. Please feel free to get benefit from it. :nerd_face:

* The fundamentals and various examples can be found in :point_right: [qlik_sense_set_analysis.md](qlik_sense_set_analysis.md) document.

* Sample data used in examples is here :point_right: [data](./data) folder.

Thanks for visiting.

Birol Mokan
